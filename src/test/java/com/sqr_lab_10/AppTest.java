package com.sqr_lab_10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;


public class AppTest {

    @Test
    public void testTitleInnopolis() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.unsplash.com/");
        

        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div/header/nav/div[2]/form/div[1]/input"))).sendKeys("Innopolis");

        WebElement searchButton = driver.findElement(By.xpath("/html/body/div/div/header/nav/div[2]/form/button"));
        searchButton.click();
        
        WebElement title = driver.findElement(By.xpath("/html/body/div/div/div[2]/div[2]/h1"));
        Assert.assertEquals(title.getText(), "Innopolis");

        System.out.println("\n\n Title: " + title.getText() + "\n\n");
        
        driver.quit();
    }
    

    @Test
    public void testPageBottom() {
        WebDriver driver = new FirefoxDriver();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        driver.get("https://www.unsplash.com/");
        
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div/header/nav/div[2]/form/div[1]/input"))).sendKeys("Innopolis");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div/div/header/nav/div[2]/form/button"));
        searchButton.click();

        jse.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div/div[2]/div[4]/p")));
        WebElement bottomLabel = driver.findElement(By.xpath("/html/body/div/div/div[2]/div[4]/p"));
        Assert.assertEquals(bottomLabel.getText(), "Make something awesome");

        System.out.println("\n\n Bottom label: " + bottomLabel.getText() + "\n\n");

        driver.quit();
    }


    @Test
    public void testImageLocation() {
        WebDriver driver = new FirefoxDriver();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        Actions actions = new Actions(driver);

        driver.get("https://www.unsplash.com/");
        
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div/header/nav/div[2]/form/div[1]/input"))).sendKeys("Innopolis");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div/div/header/nav/div[2]/form/button"));
        searchButton.click();

        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div/div[2]/div[3]/div[4]/div/div/div/div[2]/figure[4]/div/div[1]/div/div/a/div/div[2]/div/img"))).click();
        WebElement locationInfo = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[4]/div/div/div[1]/div[4]/div[3]/div/a/span/span"));

        Assert.assertEquals(locationInfo.getText(), "Innopolis, Republic of Tatarstan, Russia");


        System.out.println("\n\n Picture was taken in: " + locationInfo.getText() + "\n\n");
        
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        driver.quit();
    }
}
